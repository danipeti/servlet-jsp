package com.epam.training.servlet_jsp.exception;

public class NoMoreJediException extends Exception {

	private static final long serialVersionUID = 1L;

	public NoMoreJediException() {
		super();
	}

	public NoMoreJediException(String message) {
		super(message);
	}

}
