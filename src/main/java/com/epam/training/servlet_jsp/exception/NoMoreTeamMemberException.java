package com.epam.training.servlet_jsp.exception;

public class NoMoreTeamMemberException extends Exception {

	private static final long serialVersionUID = 1L;

	public NoMoreTeamMemberException() {
		super();
	}

	public NoMoreTeamMemberException(String message) {
		super(message);
	}

}
