package com.epam.training.servlet_jsp.exception;

public class NoMoreSoldierException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public NoMoreSoldierException() {
		super();
	}

	public NoMoreSoldierException(String message) {
		super(message);
	}

}
