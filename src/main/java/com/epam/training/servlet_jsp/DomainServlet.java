package com.epam.training.servlet_jsp;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.training.servlet_jsp.domain.Battle;

public class DomainServlet extends HttpServlet {
	private Battle battle;
	private ApplicationContext context;

	@Override
	public void init() throws ServletException {
		context = new ClassPathXmlApplicationContext("beans.xml");
		battle = context.getBean("battleOfEndor", Battle.class);
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session  = request.getSession();
		if (session.getAttribute("battle") == null) {
			session.setAttribute("battle", battle);
		}
		PrintWriter writer = response.getWriter();
		writer.println("OK");
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		battle.start();
	}

}
