package com.epam.training.servlet_jsp.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import com.epam.training.servlet_jsp.domain.Battle;

public class BattleTag extends SimpleTagSupport {
	private Battle battle;
	
	@Override
	public void doTag() throws JspException, IOException {
		getJspContext().getOut().write("Battle: " + battle.getName());
	}

	public void setBattle(Battle battle) {
		this.battle = battle;
	}
	
}
