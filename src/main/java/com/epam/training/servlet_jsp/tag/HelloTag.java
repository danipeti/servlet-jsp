package com.epam.training.servlet_jsp.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class HelloTag extends SimpleTagSupport {
	private String user;
	
	@Override
	public void doTag() throws JspException, IOException {
		getJspContext().getOut().write("Hello " + user + "!");
	}

	public void setUser(String user) {
		this.user = user;
	}
	
	

	
}
