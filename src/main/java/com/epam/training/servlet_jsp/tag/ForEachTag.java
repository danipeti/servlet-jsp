package com.epam.training.servlet_jsp.tag;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.jsp.JspContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.commons.collections.iterators.ArrayIterator;

public class ForEachTag extends SimpleTagSupport {
	
	private Object items;
	private String var;
	
	@Override
	public void doTag() throws JspException, IOException {
		for (Object item : getIterable()) {
			JspContext context = getJspContext();
			context.setAttribute(var, item);
			getJspBody().invoke(null);
		}
		
	}
	
	private Iterable<?> getIterable() {
		if (items instanceof Iterable<?>) {
			return (Iterable<?>) items;
		} else if (items instanceof Object[]) {
			return convertArrayToIterable((Object[]) items);
		} else if (items instanceof Map<?, ?>) {
			return ((Map<?, ?>)items).entrySet();
		}
		return null;
	}
	
	private Iterable<?> convertArrayToIterable(Object[] array) {
		List<Object> result = new ArrayList<>();
		ArrayIterator iterator = new ArrayIterator(array);
		while (iterator.hasNext()) {
			Object next = iterator.next();
			result.add(next);
		}
		return result;
	}
	
	
	public void setItems(Object items) {
		this.items = items;
	}
	
	public void setVar(String var) {
		this.var = var;
	}
	
	
}
