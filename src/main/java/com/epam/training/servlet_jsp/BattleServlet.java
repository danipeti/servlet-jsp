package com.epam.training.servlet_jsp;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class BattleServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/domain");
		dispatcher.include(request, response);
		
		HttpSession session = request.getSession();
		request.setAttribute("battle", session.getAttribute("battle"));
		
		RequestDispatcher viewDispatcher = request.getRequestDispatcher("battle.jsp");
		viewDispatcher.forward(request, response);
	}

	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/domain");
		dispatcher.include(request, response);
		
		HttpSession session = request.getSession();
		request.setAttribute("battle", session.getAttribute("battle"));
		
		RequestDispatcher viewDispatcher = request.getRequestDispatcher("battle.jsp");
		viewDispatcher.forward(request, response);
	}
	
}
