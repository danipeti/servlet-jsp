package com.epam.training.servlet_jsp;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FirstServlet extends HttpServlet {
	private static final long serialVersionUID = 9081635885300401980L;
	private String value;
	private String adminEmail;
	
	@Override
	public void init() throws ServletException {
		value = getServletConfig().getInitParameter("NAME");
		adminEmail = getServletContext().getInitParameter("adminEmail");
	}


	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//PrintWriter writer = response.getWriter();
		
		Date today = new Date();
		request.setAttribute("time", today);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
		dispatcher.forward(request, response);
		
		/*writer.println("<html>");
		writer.println("<head>");
		writer.println("<title>");
		writer.println("</title>");
		writer.println("</head>");
		writer.println("<body>");
		writer.println("<h1>First Servlet call!</h1>");
		writer.println("<p>" + today + "</p>");
		writer.println("<p>" + value + "</p>");
		writer.println("<p>Admin email: " + adminEmail + "</p>");
		writer.println("</body>");
		writer.println("</html>");*/
	}
	
}
