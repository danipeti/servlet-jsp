package com.epam.training.servlet_jsp;

import java.io.IOException;
import java.io.InputStream;

import javax.print.attribute.standard.MediaSize.Other;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CafeteriaServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setHeader("Content-type", "application/pdf");
		String fileName = "WEB-INF/caf.pdf";
		InputStream fileInputStream = getServletContext().getResourceAsStream(fileName);
		ServletOutputStream outputStream = response.getOutputStream();
		
		int bytes;
		while ((bytes = fileInputStream.read()) != -1) {
			outputStream.write(bytes);
		}
		
	}

}
