package com.epam.training.servlet_jsp.domain;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class LightSaber extends Weapon implements InitializingBean, DisposableBean {

	public LightSaber(String name, int damage) {
		super(name, damage);
	}

	@Override
	public String hit(Person person) {
		//person.loseLife(this.getPower());
		return "ZzzzzzzZzzzzzzzZzzzzzz";
	}

	@Override
	public String toString() {
		return "LightSaber[" + getName() + " #" + System.identityHashCode(this)%100 
				+ ", " + getPower() + "]";
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		//LOGGER.info("LightSaber["+name+"] afterPropertiesSet");
	}
	
	@Override
	public void destroy() throws Exception {
		//LOGGER.info("LightSaber["+name+"] destroy");
	}

}
