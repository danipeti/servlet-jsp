package com.epam.training.servlet_jsp.domain;


public class Sith extends ForceUser {
	
	public Sith(final String name, final Gender gender) {
		super(name, gender, Force.DARK);
	}
		
	@Override
	public String toString() {
		return "Sith[" + getName() + ", " + getGender() + ", " + getLifePoints() + ", " + getLightSaber() + "]";
	}

}
