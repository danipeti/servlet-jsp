package com.epam.training.servlet_jsp.domain;

public class Trooper extends Person {
	private Weapon weapon;

	public Trooper(final String name, final Gender gender) {
		super(name, gender);
	}
	
	@Override
	public String fight(final Person person) {
		int myPower = this.weapon.getPower();
		int enemyPower = 0;

		if (person instanceof Trooper) {
			Trooper enemy = (Trooper)person;
			enemyPower = enemy.getWeapon().getPower();
		}
		else if (person instanceof ForceUser) {
			ForceUser enemy = (ForceUser)person;
			enemyPower = enemy.getLightSaber().getPower();
		}
			
		if (enemyPower >= myPower) {
			this.loseLife(enemyPower);
		} else if (myPower >= enemyPower) {
			person.loseLife(myPower);
		}

		return weapon.hit(person);
	}
	
	public Weapon getWeapon() {
		return weapon;
	}

	public void setWeapon(Weapon weapon) {
		this.weapon = weapon;
	}

	@Override
	public String toString() {
		return "Trooper[" + getName() + ", " + getGender() + ", " + getWeapon() + "]";
	}

	
	
}
