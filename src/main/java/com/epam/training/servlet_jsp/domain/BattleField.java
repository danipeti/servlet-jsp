package com.epam.training.servlet_jsp.domain;

import java.util.Arrays;

public class BattleField {
	private final String name;
	private final Army[] armies;

	public BattleField(final String name, final Army[] armies) {
		this.name = name;
		this.armies = armies;
	}

	public String getName() {
		return name;
	}

	public Army[] getArmies() {
		return armies;
	}

	@Override
	public String toString() {
		return "BattleField [name=" + name + ", armies="
				+ Arrays.toString(armies) + "]";
	}
	
	
}
