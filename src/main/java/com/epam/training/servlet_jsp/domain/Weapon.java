package com.epam.training.servlet_jsp.domain;

public abstract class Weapon {
	private String name;
	private final int power;

	public Weapon(final String name, final int power) {
		this.name = name;
		this.power = power;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPower() {
		return power;
	}
	
	public abstract String hit(final Person person);

	@Override
	public String toString() {
		return "Weapon[" + name + ", " + power + "]";
	}
	
}
