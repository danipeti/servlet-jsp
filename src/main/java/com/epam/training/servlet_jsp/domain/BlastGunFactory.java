package com.epam.training.servlet_jsp.domain;

import org.springframework.beans.factory.FactoryBean;

public class BlastGunFactory implements FactoryBean<BlastGun> {
	private final int damageRange;
	
	public BlastGunFactory(final int damageRange) {
		this.damageRange = damageRange;
	}

	@Override
	public BlastGun getObject() throws Exception {
		String name = "BlastGun #" + System.nanoTime();
		int damage = (int) (Math.random() * damageRange);
		return new BlastGun(name, damage);
	}

	@Override
	public Class<BlastGun> getObjectType() {
		return BlastGun.class;
	}

	@Override
	public boolean isSingleton() {
		return false;
	}
	
}
