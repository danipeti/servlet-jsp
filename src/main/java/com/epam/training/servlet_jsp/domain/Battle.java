package com.epam.training.servlet_jsp.domain;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;

public class Battle {
	private final String name;
	private final BattleField battleField;
	
	private MessageSource messageSource;
	private Locale locale = Locale.getDefault();
	
	private static Logger LOGGER = LoggerFactory.getLogger(Battle.class);

	public Battle(final String name, final BattleField battleField) {
		this.name = name;
		this.battleField = battleField;
	}

	public void start() {
		LOGGER.info(messageSource.getMessage("battle.startgame", new Object[] { name }, getLocale()));

		Thread soldierThread = new Thread(new SoldierFight(battleField, messageSource), "Soldiers");
		Thread jediThread = new Thread(new JediFight(battleField, messageSource), "Jedis");
		soldierThread.start();
		jediThread.start();
		
		try {
			soldierThread.join();
			jediThread.join();
		} catch (InterruptedException e) {
			LOGGER.info("Error while waiting for the final battle.");
		}
		
		
		Thread jediSoldierThread = new Thread(new JediSoldierFight(battleField, messageSource), "Jedi-Soldier");
		jediSoldierThread.start();
		
		try {
			jediSoldierThread.join();
		} catch (InterruptedException e) {
			LOGGER.info("Error while waiting for the end of battle.");
		}
		
		LOGGER.info(messageSource.getMessage("battle.endmessage", null, getLocale()));
		
	}


	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public String getName() {
		return name;
	}
	public BattleField getBattleField() {
		return battleField;
	}
	
	@Override
	public String toString() {
		return "Battle [name=" + name + ", battleField=" + battleField + "]";
	}
	
}
