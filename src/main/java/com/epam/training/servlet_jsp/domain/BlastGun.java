package com.epam.training.servlet_jsp.domain;

public class BlastGun extends Weapon {

	public BlastGun(String name, int damage) {
		super(name, damage);
	}

	@Override
	public String hit(Person person) {
		return "puf-puf";
	}

	@Override
	public String toString() {
		return "BlastGun[" + getName() + ", " + getPower() + "]";
	}
	
}
