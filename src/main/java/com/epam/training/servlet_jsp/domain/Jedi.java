package com.epam.training.servlet_jsp.domain;



public class Jedi extends ForceUser {
	
	public Jedi(final String name, final Gender gender) {
		super(name, gender, Force.LIGHT);
	}
	
	@Override
	public String toString() {
		return "Jedi[" + getName() + ", " + getGender() + ", " + getLifePoints() + ", " + getLightSaber() + "]";
	}

}
