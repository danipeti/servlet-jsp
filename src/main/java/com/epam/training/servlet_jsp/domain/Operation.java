package com.epam.training.servlet_jsp.domain;

public enum Operation {
	OFFENCE,
	DEFENCE;
}
