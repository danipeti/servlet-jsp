package com.epam.training.servlet_jsp.domain;

public abstract class Person {
	private final String name;
	private final Gender gender;
	private int lifePoints = 100;
	
	public Person(final String name, final Gender gender) {
		this.name = name;
		this.gender = gender;
	}
	
	
	public void loseLife(int damage) {
		lifePoints -= damage;
		if (lifePoints < 0) {
			lifePoints = 0;
		}
	}
	
	
	public boolean isDead() {
		return lifePoints <= 0;
	}
	
	public String getName() {
		return name;
	}
	
	public Gender getGender() {
		return gender;
	}

	public int getLifePoints() {
		return lifePoints;
	}
	
	public abstract String fight(final Person target);
}
