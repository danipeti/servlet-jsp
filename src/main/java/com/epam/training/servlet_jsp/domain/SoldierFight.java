package com.epam.training.servlet_jsp.domain;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;

import com.epam.training.servlet_jsp.exception.NoMoreSoldierException;

public class SoldierFight implements Runnable {
	private BattleField battleField;

	private MessageSource messageSource;
	private Locale locale = Locale.getDefault();
	
	private static Logger LOGGER = LoggerFactory.getLogger(SoldierFight.class);


	public SoldierFight(BattleField battleField, MessageSource messageSource) {
		this.battleField = battleField;
		this.messageSource = messageSource;
	}

	@Override
	public void run() {
		Army[] armies = battleField.getArmies();
		
		boolean endOfFight = false;
		while (!endOfFight) {
			int i1 = (int) (Math.random() * 2);
			int i2 = 1 - i1;
			Army army1 = armies[i1];
			Army army2 = armies[i2];
			
			try {
				Person soldier1 = army1.getRandomSoldier(Operation.OFFENCE, Operation.DEFENCE);
				Person soldier2 = army2.getRandomSoldier(Operation.DEFENCE, Operation.OFFENCE);
				String fight = soldier1.fight(soldier2);
				LOGGER.info(fight);
			} catch (NoMoreSoldierException e) {
				endOfFight = true;
				LOGGER.info(messageSource.getMessage("battle.endOfSoldiersFight", null, locale));
			}
			
			try {
				Thread.sleep((long) (Math.random() * 10));
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}

	}

	
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
}
