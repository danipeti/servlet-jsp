package com.epam.training.servlet_jsp.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.context.MessageSource;

import com.epam.training.servlet_jsp.exception.NoMoreJediException;
import com.epam.training.servlet_jsp.exception.NoMoreSoldierException;
import com.epam.training.servlet_jsp.exception.NoMoreTeamMemberException;

public class Army {
	private ForceUser leader;
	private List<ForceUser> jedis = new ArrayList<ForceUser>();
	private Map<Operation, List<Person>> soldiers = new EnumMap<Operation, List<Person>>(Operation.class);
	
	private MessageSource messageSource;
	private Locale locale = Locale.getDefault();
	
	
	public Army(final ForceUser leader, Map<Operation, List<Person>> soldiers) {
		this.leader = leader;
		this.soldiers.putAll(soldiers);
	}

	public Person getRandomSoldier(Operation... operations) throws NoMoreSoldierException {
		Person result = null;
		
		for (Operation operation : operations) {
			List<Person> soldierList = new ArrayList<Person>();
			for (Person person : soldiers.get(operation)) {
				if (!person.isDead()) {
					soldierList.add(person);
				}
			}
			if (soldierList.size() > 0) {
				result = soldierList.get((int) (Math.random() * soldierList.size()));
			}
		}
		
		if (result == null) {
			throw new NoMoreSoldierException("no more soldiers");			
		}
		return result;
	}

	public ForceUser getRandomJedi() throws NoMoreJediException {
		List<ForceUser> jedis = new ArrayList<ForceUser>();
		for (ForceUser jedi : this.jedis) {
			if (!jedi.isDead()) {
				jedis.add(jedi);
			}
		}
		if (!leader.isDead()) {
			jedis.add(leader);
		}
		if (jedis.size() == 0) {
			throw new NoMoreJediException("no more jedis");
		}
		ForceUser result = jedis.get((int) (Math.random() * jedis.size()));
		return result;
	}
	
	public Person getRandomArmyMember() throws NoMoreTeamMemberException {
		List<Person> team = new ArrayList<Person>();
		for (Person jedi : this.jedis) {
			if (!jedi.isDead()) {
				team.add(jedi);
			}
		}
		for (Person defender : getDefenders()) {
			if (!defender.isDead()) {
				team.add(defender);
			}
		}
		for (Person offender : getOffenders()) {
			if (!offender.isDead()) {
				team.add(offender);
			}
		}

		if (team.size() == 0) {
			throw new NoMoreTeamMemberException("no more team members");
		}
		Person result = team.get((int) (Math.random() * team.size()));
		return result;
		
	}
	
	public Map<Operation, List<Person>> getSoldiers() {
		return Collections.unmodifiableMap(soldiers);
	}
	
	public List<Person> getDefenders() {
		return Collections.unmodifiableList(soldiers.get(Operation.DEFENCE));
	}
	
	public List<Person> getOffenders() {
		return Collections.unmodifiableList(soldiers.get(Operation.OFFENCE));
	}
	
	
	public Person getLeader() {
		return leader;
	}
	
	public void setJedis(List<ForceUser> jedis) {
		this.jedis.clear();
		this.jedis.addAll(jedis);
	}

	public List<ForceUser> getJedis() {
		//return Collections.unmodifiableList(jedis);
		return jedis;
	}
	
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("\n" + messageSource.getMessage("army.army", null, locale));
		sb.append("\n" + messageSource.getMessage("army.leader", null, locale) + ": ");
		sb.append(leader.toString() + "\n");
		for (ForceUser j : jedis) {
			sb.append(j.toString() + "\n");
		}
		List<Person> offenders = soldiers.get(Operation.OFFENCE);
		int offenderSize = offenders.size();
		sb.append(offenderSize + " ");
		sb.append(messageSource.getMessage("army.offenders", null, locale) +  " [");
		for (Person offender : offenders) {
			sb.append(offender.getLifePoints() + ",");
		}
		sb.append("], ");
		
		List<Person> defenders = soldiers.get(Operation.DEFENCE);
		int defenderSize = defenders.size();
		sb.append(defenderSize + " ");
		sb.append(messageSource.getMessage("army.defenders", null, locale) +  " [");
		for (Person defender : defenders) {
			sb.append(defender.getLifePoints() + ",");
		}
		sb.append("]\n");
		return sb.toString();
	}


}
