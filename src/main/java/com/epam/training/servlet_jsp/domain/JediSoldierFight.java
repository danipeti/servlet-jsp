package com.epam.training.servlet_jsp.domain;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;

import com.epam.training.servlet_jsp.exception.NoMoreTeamMemberException;

public class JediSoldierFight implements Runnable{
	private BattleField battleField;
	
	private MessageSource messageSource;
	private Locale locale = Locale.getDefault();
	
	private static Logger LOGGER = LoggerFactory.getLogger(JediSoldierFight.class);
	
	public JediSoldierFight(BattleField battleField, MessageSource messageSource) {
		this.battleField = battleField;
		this.messageSource = messageSource;
	}
	

	@Override
	public void run() {
		Army[] armies = battleField.getArmies();
		
		boolean endOfFight = false;
		while (!endOfFight) {
			int i1 = (int) (Math.random() * 2);
			int i2 = 1 - i1;
			Army army1 = armies[i1];
			Army army2 = armies[i2];
			
			try {
				Person member1 = army1.getRandomArmyMember();
				Person member2 = army2.getRandomArmyMember();
				String fight = member1.fight(member2);
				LOGGER.info(fight);
			} catch (NoMoreTeamMemberException e) {
				endOfFight = true;
				LOGGER.info(messageSource.getMessage("battle.endOfJedisSoldiersFight", null, locale));
			}

			try {
				Thread.sleep((long) (Math.random() * 5));
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}

		}


	}

}
