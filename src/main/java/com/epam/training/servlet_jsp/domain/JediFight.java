package com.epam.training.servlet_jsp.domain;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;

import com.epam.training.servlet_jsp.exception.NoMoreJediException;

public class JediFight implements Runnable {
	private BattleField battleField;
	
	private MessageSource messageSource;
	private Locale locale = Locale.getDefault();
	
	private static Logger LOGGER = LoggerFactory.getLogger(JediFight.class);


	public JediFight(BattleField battleField, MessageSource messageSource) {
		this.battleField = battleField;
		this.messageSource = messageSource;
	}

	@Override
	public void run() {
		Army[] armies = battleField.getArmies();
		
		boolean endOfFight = false;
		while (!endOfFight) {
			int i1 = (int) (Math.random() * 2);
			int i2 = 1 - i1;
			Army army1 = armies[i1];
			Army army2 = armies[i2];
			
			try {
				ForceUser jedi1 = army1.getRandomJedi();
				ForceUser jedi2 = army2.getRandomJedi();
				String fight = jedi1.fight(jedi2);
				LOGGER.info(fight);
			} catch (NoMoreJediException e) {
				endOfFight = true;
				LOGGER.info(messageSource.getMessage("battle.endOfJedisFight", null, locale));
			}

			try {
				Thread.sleep((long) (Math.random() * 25));
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}

		}

	}
	
	
	
}
