package com.epam.training.servlet_jsp.domain;

public abstract class ForceUser extends Person {
	private Force allegiance = Force.UNDECIDED;
	private LightSaber lightSaber;
	
	public ForceUser(final String name, final Gender gender) {
		super(name, gender);
	}
	
	public ForceUser(final String name, final Gender gender, final Force allegiance) {
		this(name, gender);
		this.allegiance = allegiance;
	}
	

	public String fight(final Person person) {
		int myPower = this.lightSaber.getPower();
		int enemyPower = 0;

		if (person instanceof Trooper) {
			Trooper enemy = (Trooper)person;
			enemyPower = enemy.getWeapon().getPower();
		}
		else if (person instanceof ForceUser) {
			ForceUser enemy = (ForceUser)person;
			enemyPower = enemy.getLightSaber().getPower();
		}
			
		if (enemyPower >= myPower) {
			this.loseLife(enemyPower);
		} else if (myPower >= enemyPower) {
			person.loseLife(myPower);
		}

		return lightSaber.hit(person);
	}
	
	
	public void setAllegiance(Force allegiance) {
		this.allegiance = allegiance;
	}

	public Force getAllegiance() {
		return allegiance;
	}

	public void setLightSaber(final LightSaber lightSaber) {
		this.lightSaber = lightSaber;
	}

	public LightSaber getLightSaber() {
		return lightSaber;
	}

	
}
