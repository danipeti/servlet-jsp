package com.epam.training.servlet_jsp.domain;

public enum Force {
	LIGHT,
	DARK, 
	UNDECIDED;
}
