<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Time</title>
</head>
<body>

<h1>Time</h1>
<p>Old time: <%= request.getAttribute("oldTime") %></p>
<p>News time: <%= request.getAttribute("newTime") %></p>

</body>
</html>