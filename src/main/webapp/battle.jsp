<%@ page isELIgnored="false" errorPage="error.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="my" uri="myTags" %>

<html>
<%@ include file="WEB-INF/jspf/header.jspf" %>
<body>
<%-- 	<pre><%= request.getAttribute("battle") %></pre> --%>
<%-- 	<my:hello user="Johnny" /><br/> --%>
<%-- 	<my:battle battle="${battle}"/> --%>
	
	<div class="container">
		<h2>${battle.name}</h2><br/>
		
		<div class="row">
			<my:forEach var="army" items="${battle.battleField.armies}">
				<div class="col-md-6">
					<table class="table text-center">
						<tr>
							<th class="text-center">Name</th>
							<th class="text-center">Gender</th>
							<th class="text-center">Life points</th>
							<th class="text-center">Weapon power</th>
						</tr>
					
						<tr>
							<td class="text-left">${army.leader.name}</td>
							<td>${army.leader.gender}</td>
							<td>${army.leader.lifePoints}</td>
							<td>${army.leader.lightSaber.power}</td>
						</tr>
						
						<my:forEach var="jedi" items="${army.jedis}">
							<tr>
								<td class="text-left">${jedi.name}</td>
								<td>${jedi.gender}</td>
								<td>${jedi.lifePoints}</td>
								<td>${jedi.lightSaber.power}</td>
							</tr>
						</my:forEach>
						
						<my:forEach var="map" items="${army.soldiers}">
							<my:forEach var="soldier" items="${map.value}">
								<tr>
									<td class="text-left">${soldier.name}</td>
									<td>${soldier.gender}</td>
									<td>${soldier.lifePoints}</td>
									<td>${soldier.weapon.power}</td>
								</tr>
							</my:forEach>
						</my:forEach>
					</table>
				</div>
			</my:forEach>
		</div>
		
		<form action="" method="post">
			<input class="btn btn-primary btn-lg" type="submit" value="Fight">
		</form>
		
		<%@ include file="WEB-INF/jspf/footer.jspf" %>
	</div>
</body>
</html>
