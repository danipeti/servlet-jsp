<html>
<head>
	<title>Index</title>
</head>
<body>
	<h2>Hello World!</h2>
	<p>Time: <%= request.getAttribute("time") %></p>
	<p>Admin email: <%= getServletContext().getInitParameter("adminEmail") %></p>
	<p>JSP init param: <%= getServletConfig().getInitParameter("jspInitParam") %></p>
</body>
</html>
